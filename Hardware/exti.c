#include "exti.h"
#include "gpio_led.h"


/**
  * @brief  Configure EXTI_GPIO working mode
  * @param  None
  * @return None
  */
 void EXTI_GPIO_Init(void)
 {
   /* Enable GPIOA clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_BMX1 |RCC_APB1Periph_GPIOA , ENABLE);
   /* 配置为开漏输出，必须要接上拉电阻。在开漏模式时，对输入数据寄存器的读访问可得到I/O状态 */
  GPIO_Init(GPIOA, GPIO_Pin_0  , GPIO_MODE_IN | GPIO_PUPD_DOWN);
  GPIO_Init(GPIOA, GPIO_Pin_1  , GPIO_MODE_IN | GPIO_PUPD_UP);
 }

/**
  * @brief  Configure PA0 in interrupt mode
  * @param  None
  * @return None
  */
void EXTI_Config(void)
{
  /* Private variables ---------------------------------------------------------*/
  EXTI_InitTypeDef   EXTI_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  EXTI_GPIO_Init();

  /* Enable AFIO clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_BMX1 |RCC_APB1Periph_EXTI |RCC_APB1Periph_AFIO, ENABLE);

  /* Connect EXTI Line to SDA pin */
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0 );
  
  /* Connect EXTI Line to SCL pin */
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource1);

  /* Configure EXTI line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line0;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Configure and enable EXTI interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
//  /* Configure EXTI line */
//  EXTI_InitStructure.EXTI_Line = EXTI_Line1;
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);

//  /* Configure and enable EXTI interrupt */
//  NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);

  /* Generate software interrupt: simulate a falling edge applied on EXTI0 line */
  EXTI_GenerateSWInterrupt(EXTI_Line0);
  
  /* Generate software interrupt: simulate a falling edge applied on EXTI1 line */
  EXTI_GenerateSWInterrupt(EXTI_Line1);
}

void Exti_Init(void)
{
  /* Configure interrupt mode */
  EXTI_Config();
}



/**
  * @brief  This function handles External line 0 interrupt request.
  * @param  None
  * @return None
  */
void EXTI0_IRQHandler(void)
{
  RCC_APB1CLKConfig(RCC_MAINCLK_Div1, ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_BMX1 |RCC_APB1Periph_GPIOB |RCC_APB1Periph_EXTI, ENABLE);
  
  if(EXTI_GetITStatus(EXTI_Line0) != RESET)
  {    
    /* Clear the  EXTI line 0 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line0);
  }
}

/**
  * @brief  This function handles External lines 1 interrupt request.
  * @param  None
  * @return None
  */
void EXTI1_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line1) != RESET)
  {
    /* Clear the EXTI line 1 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line1);
  }
}




