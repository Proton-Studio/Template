#ifndef __TIMER_H
#define __TIMER_H
#include "wb32f10x_it.h"

extern uint32_t timer_count;

void TIM3_Int_Init(uint16_t arr , uint16_t psc);

#endif

