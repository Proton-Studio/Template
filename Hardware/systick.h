#ifndef __SYSTICK_H
#define __SYSTICK_H
#include "wb32f10x_it.h"


void SysTick_Init(void);
void delay_us(__IO uint32_t nTime);
void delay_ms(__IO uint32_t nTime);

void Led_Init(void);

#endif

