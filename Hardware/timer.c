#include "timer.h"
#include "gpio_led.h"
uint32_t timer_count = 0;

/**
  * @brief  TIM3 Configuration
  * @param  arr:预分频值  psc:预装载值
  * @return None
  */
void TIM3_Int_Init(uint16_t arr,uint16_t psc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); 
	
	TIM_TimeBaseStructure.TIM_Period = arr; 
	TIM_TimeBaseStructure.TIM_Prescaler =psc; 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
 
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE ); 


	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;  
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;  
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
	NVIC_Init(&NVIC_InitStructure); 


	TIM_Cmd(TIM3, ENABLE); 			 
}

/**
  * @brief  TIM3 interrupt funcation in 10ms/times
  * @param  None
  * @return None
  */
void TIM3_IRQHandler(void)   
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)  
  {
    timer_count++;
    timer_count %= 100000000;
    if(timer_count%10==0)
    {
      static uint8_t led_flash = 0;
      LED1 = !led_flash;
      LED2 = led_flash;
      led_flash = !led_flash;
    }
    TIM_ClearITPendingBit(TIM3, TIM_IT_Update  );  
  }
}












