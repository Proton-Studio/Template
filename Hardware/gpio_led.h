#ifndef __GPIO_LED_H
#define __GPIO_LED_H
#include "wb32f10x_it.h"

#define LED_ON  0
#define LED_OFF 1


#define LED1  PBout(13)
#define LED2  PBout(14)


void GPIO_Led_Init(void);

#endif

