/* Includes ------------------------------------------------------------------*/
#include "wb32f10x.h"
#include "systick.h"
#include "timer.h"
#include "usart.h"
#include "exti.h"
#include "gpio_led.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "led.h"
#include "rtc.h"
#include "adc.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
extern void Clock_Config( void );
/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main( void )
{
  SystemCoreClockUpdate();
  NVIC_PriorityGroupConfig( NVIC_PriorityGroup_2 );
  ADC_DMA_Init();
  Led_Init();
  GPIO_Led_Init();
  Exti_Init();
  usart1_Init( 115200 );
  TIM3_Int_Init( 99, 9599 ); //10ms
  SysTick_Init();
  Rtc_Init();
  puts( "Wecome to my world , start your act.\r\n" );
  RTC_SetAlarm( RTC_GetCounter() + 1 );
  RTC_WaitForLastTask();    //等待最近一次对RTC寄存器的写操作完成
  /* Infinite loop */
  while( 1 )
  {

    if( Usart1RecData.rec_ok )
    {
      if( ( Usart1RecData.rxIndex == 28 ) && ( strstr( ( const char* )Usart1RecData.rxBuffer, "set_time:" ) != NULL ) )
      {
//        _calendar_obj T_calendar = {0};

//        T_calendar.w_year  = ( uint16_t )atoi( ( const char* )&Usart1RecData.rxBuffer[9] );
//        T_calendar.w_month = ( uint8_t ) atoi( ( const char* )&Usart1RecData.rxBuffer[14] );
//        T_calendar.w_date  = ( uint8_t ) atoi( ( const char* )&Usart1RecData.rxBuffer[17] );
//        T_calendar.hour    = ( uint8_t ) atoi( ( const char* )&Usart1RecData.rxBuffer[20] );
//        T_calendar.min     = ( uint8_t ) atoi( ( const char* )&Usart1RecData.rxBuffer[23] );
//        T_calendar.sec     = ( uint8_t ) atoi( ( const char* )&Usart1RecData.rxBuffer[26] );

//        if( !RTC_Set( T_calendar.w_year, T_calendar.w_month, T_calendar.w_date, T_calendar.hour, T_calendar.min, T_calendar.sec ) ) //设置时间
//        {
//          puts( "Set OK.\r\n" );
//        }
//        else
//        {
//          puts( "Set failed , The time format is incorrect.\r\n" );
//        }
      }

//      Uart1Send(Usart1RecData.rxBuffer, Usart1RecData.rxIndex);
      Usart1RecData.rec_ok = 0;
      Usart1RecData.rxIndex = 0;
    }

    if( timer_count % 100 == 0 )
    {
      timer_count++;
      printf( "\r\rThe RP1 voltage values is: %.2f V", 3.3 * GET_ADC_AverageValue() / 4095 );
      printf( "\r\rChip Temperature: %.2f", ( float )GET_ADC_TemperatureAvgVal() );
    }
  }
}



#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed( uint8_t* file, uint32_t line )
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while( 1 )
  {
  }
}
#endif
