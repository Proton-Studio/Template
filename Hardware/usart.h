#ifndef __USART_H
#define __USART_H
#include "wb32f10x_it.h"

#define BUFFERLEN  100

typedef struct
{
  uint32_t rxIndex ;
  uint8_t  rec_ok;
  uint8_t  rxBuffer[BUFFERLEN];
  
}RECDATA;
typedef struct
{
  uint32_t txIndex ;
  uint32_t txLen;
  uint8_t  txBuffer[BUFFERLEN];
}SENDDATA;

extern RECDATA Usart1RecData;
extern SENDDATA Usart1SendData;

void usart1_Init(uint32_t baud);
void Uart1Send(const uint8_t* txBuf, uint32_t txLen);
void Uart1ITSend(const uint8_t* txBuf, uint32_t txLen);
#endif

