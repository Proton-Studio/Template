#include "usart.h"
#include "stdio.h"
#include "string.h"
/* Private variables ---------------------------------------------------------*/
RECDATA Usart1RecData={0};
SENDDATA Usart1SendData={0,0,"UART Interrupt Example"};

/* Private functions ---------------------------------------------------------*/
void usart1_Init(uint32_t baud)
{
  UART_InitTypeDef UART_InitStructure;
  NVIC_InitTypeDef  NVIC_InitStructure;
  
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  
  /* Enable GPIOA and UART1 clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_BMX1 |RCC_APB1Periph_GPIOA |RCC_APB1Periph_UART1, ENABLE);

  /*
    PA9  (UART1_TX)
    PA10 (UART1_RX)
  */
  GPIO_Init(GPIOA, GPIO_Pin_9 |GPIO_Pin_10, GPIO_MODE_AF |GPIO_OTYPE_PP |GPIO_PUPD_UP |GPIO_SPEED_HIGH |GPIO_AF7);

  /* UART1 configuration */
  UART_DeInit(UART1);
  UART_InitStructure.UART_BaudRate = baud;
  UART_InitStructure.UART_WordLength = UART_WordLength_8b;
  UART_InitStructure.UART_StopBits = UART_StopBits_One;
  UART_InitStructure.UART_Parity = UART_Parity_None;
  UART_InitStructure.UART_AutoFlowControl = UART_AutoFlowControl_None;
  UART_Init(UART1, &UART_InitStructure);

  /* Enable the UART1 Interrupt Channel */
  NVIC_InitStructure.NVIC_IRQChannel = UART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  UART_ITConfig(UART1, UART_IT_RDA, ENABLE);
//  UART_ITConfig(UART1, UART_IT_THRE, ENABLE);

  UART_RxFIFOThresholdConfig( UART1 , UART_RxFIFOThreshold_4);
  UART_FIFOCmd(UART1, ENABLE);
}

void Uart1ITSend(const uint8_t* txBuf, uint32_t txLen)
{
  memcpy(Usart1SendData.txBuffer,txBuf,txLen);
  Usart1SendData.txLen = txLen;
  Usart1SendData.txIndex = 0;
  
  UART_ITConfig(UART1, UART_IT_THRE, ENABLE);
}
void Uart1Send(const uint8_t* txBuf, uint32_t txLen)
{
  while(txLen--)
  {
    /* Loop until the THR or TX FIFO is empty */
    while(!(UART_GetLineStatus(UART1) & UART_LINE_STATUS_THRE));

    UART_WriteData(UART1, *txBuf++);
  }
  /* Loop until the end of transmission */
  while(!(UART_GetLineStatus(UART1) & UART_LINE_STATUS_TEMT));
}
void UART1_IRQHandler(void)
{
  uint8_t rbyte;
  uint8_t int_id;

  int_id = UART_GetIntID(UART1);
  
  if(int_id == UART_INTID_CTI)   //FIFO Time out
  {
    while(UART_GetFlagStatus(UART1, UART_FLAG_RFNE) != RESET)
    {
      rbyte = UART_ReadData(UART1);
      if(Usart1RecData.rxIndex < BUFFERLEN)
      {
        Usart1RecData.rxBuffer[Usart1RecData.rxIndex++] = rbyte;
//        if(rxIndex >= BUFFERLEN) break;
      }
    }
    Usart1RecData.rec_ok = 1;
  }
  else  if(int_id == UART_INTID_RDA)   //Receive One Byte
  {
    rbyte = UART_ReadData(UART1);
    
    if (Usart1RecData.rxIndex < BUFFERLEN) 
    {
      Usart1RecData.rxBuffer[Usart1RecData.rxIndex++] = rbyte;
      if (Usart1RecData.rxIndex >= BUFFERLEN) 
      {
        Usart1RecData.rec_ok = 1;
      }
    }
  }
  else if (int_id == UART_INTID_THRE)   //Send One Byte
  {
    if (Usart1SendData.txIndex < Usart1SendData.txLen) 
    {
      UART_WriteData(UART1, Usart1SendData.txBuffer[Usart1SendData.txIndex]);
      Usart1SendData.txIndex++;
    }
    else 
    {
      UART_ITConfig(UART1, UART_IT_THRE, DISABLE);
    }
  }
}

/* Start Retarget UART1 */

#pragma import(__use_no_semihosting_swi)

struct __FILE
{
  int handle; /* Add whatever you need here */
};
FILE __stdout;
FILE __stdin;



int fputc( int ch, FILE* f )
{
  while( !( UART1->LSR & UART_LSR_THRE ) );
  UART1->THR = ch;
  while( !( UART1->LSR & UART_LSR_TEMT ) );
  return ch;
}

int fgetc( FILE* f )
{
  while( !( UART1->LSR & UART_LSR_DR ) );
  return ( UART1->RBR );
}


int ferror( FILE* f )
{
  /* Your implementation of ferror */
  return EOF;
}


void _ttywrch( int ch )
{
}

int __backspace()
{
  return 0;
}

void _sys_exit( int return_code )
{
  while( 1 );   /* endless loop */
}

/* NED Retarget UART1 */
