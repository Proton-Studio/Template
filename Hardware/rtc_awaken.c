#include "rtc_awaken.h"
#include "stdio.h"
#include "led.h"
#include "gpio_led.h"
#include "systick.h"


void enter_stop(void)
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_BKP, ENABLE); //使能BKP外设时钟
  PWR_BackupAccessCmd(ENABLE);                      //使能后备寄存器访问
  RTC_WaitForSynchro();
  RTC_WaitForLastTask();
  RTC_SetCounter(10);
  RTC_WaitForLastTask();
  RTC_SetAlarm(20);
  RTC_WaitForLastTask();

  PWR_EnterSTOPMode(PWR_STOPMode_LP4_S32KOFF, PWR_EntryMode_WFI);
  Clock_Config();
}


void Clock_Config( void )
{
  /* Enable APB1CLK. APB1CLK = MAINCLK */
  RCC_APB1CLKConfig( RCC_MAINCLK_Div1, ENABLE );

  /* Enable BMX1 and GPIOD clock */
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_BMX1 | RCC_APB1Periph_GPIOD, ENABLE );

  /* Configure PD0 and PD1 to analog mode */
  GPIO_Init( GPIOD, GPIO_Pin_0 | GPIO_Pin_1, GPIO_MODE_ANA );

  /* Disable ANCTL register write-protection */
  PWR_UnlockANA();

  /* Enable HSE */
  ANCTL_HSEConfig( ANCTL_HSE_ON );

  /* Wait till HSE is ready */
  if( ANCTL_WaitForHSEStartUp() == ERROR )
  {
    /* HSE failed */
    while( 1 );
  }

  /* Configure Flash prefetch, Cache and wait state */
  CACHE->CR = CACHE_CR_CHEEN | CACHE_CR_PREFEN_ON | CACHE_CR_LATENCY_3WS;

  /* AHBCLK = MAINCLK */
  RCC_AHBCLKConfig( RCC_MAINCLK_Div1 );

  /* Enable APB2CLK. APB2CLK = MAINCLK */
  RCC_APB2CLKConfig( RCC_MAINCLK_Div1, ENABLE );

  /* PLL configuration: PLLCLK = HSE(8MHz) * 12 = 96 MHz */
  RCC_PLLSourceConfig( RCC_PLLSource_HSE_Div1, ENABLE );
  ANCTL_PLLConfig( ANCTL_PLLMul_12 );

  /* Enable PLL */
  ANCTL_PLLCmd( ENABLE );

  /* Wait till PLL is ready */
  while( ANCTL_GetFlagStatus( ANCTL_FLAG_PLLRDY ) == RESET );

  /* Enable ANCTL register write-protection */
  PWR_LockANA();

  /* Select PLL as system clock source */
  RCC_MAINCLKConfig( RCC_MAINCLKSource_PLLCLK );

  /*Enable peripheral clock*/

  RCC_APB1PeriphClockCmd( RCC_APB1Periph_BMX1, ENABLE );
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_BMX2, ENABLE );

  RCC_APB1PeriphClockCmd( RCC_APB1Periph_GPIOA | RCC_APB1Periph_GPIOB | RCC_APB1Periph_GPIOC | RCC_APB1Periph_GPIOD, ENABLE );

  RCC_APB1PeriphClockCmd( RCC_APB1Periph_AFIO, ENABLE );

  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_DMAC1Bridge, ENABLE );
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_DMAC1, ENABLE );
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_UART1, ENABLE );
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_EXTI, ENABLE );
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM3, ENABLE );
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_ADC, ENABLE );
  
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_LED, ENABLE );

}

// 唤醒单片机使用
void __awaken_( void )
{
  /* AHBCLK = MAINCLK */
  RCC_AHBCLKConfig( RCC_MAINCLK_Div1 );
  /* Enable APB1CLK. APB1CLK = MAINCLK */
  RCC_APB1CLKConfig( RCC_MAINCLK_Div1, ENABLE );
  /* Enable APB2CLK. APB2CLK = MAINCLK */
  RCC_APB2CLKConfig( RCC_MAINCLK_Div1, ENABLE );

  RCC_APB1PeriphClockCmd( RCC_APB1Periph_BMX1 | RCC_APB1Periph_EXTI  | RCC_APB1Periph_AFIO  |  \
                          RCC_APB1Periph_GPIOA | RCC_APB1Periph_GPIOB, ENABLE );

  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_BKP, ENABLE ); //使能BKP外设时钟
  PWR_BackupAccessCmd( ENABLE );                    //使能后备寄存器访问
  RTC_WaitForSynchro();
//  RTC_WaitForLastTask();
//  RTC_SetCounter( 10 );
//  RTC_WaitForLastTask();
//  RTC_SetAlarm( 20 );
//  RTC_WaitForLastTask();
}


/**
  * @brief  Configures the nested vectored interrupt controller.
  * @param  None
  * @retval None
  */
void NVIC_RTC_Configuration( void )
{
  NVIC_InitTypeDef NVIC_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;

  /* Configure EXTI line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init( &EXTI_InitStructure );

  /* Enable the RTC Alarm Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = RTCAlarm_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init( &NVIC_InitStructure );
}

void RTC_Configuration( void )
{
  /* Reset Backup Domain */
  BKP_DeInit();
  
  /*Unlock ANA*/
  PWR_UnlockANA();
  /* Enable LSI */
  ANCTL_LSICmd( ENABLE );
  /* Lock ANA */
  PWR_LockANA();
  while( ( ANCTL->LSISR & ANCTL_LSISR_LSIRDY ) == 0 )
  {
    ;
  }
  /* Select LSIas RTC Clock Source */
  BKP_RTCCLKConfig( BKP_RTCCLKSource_LSI );

  /* Enable RTC Clock */
  BKP_RTCCLKCmd( ENABLE );

  /* Wait for RTC registers synchronization */
  RTC_WaitForSynchro();

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  RTC_ITConfig( RTC_IT_ALR, ENABLE );

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  /* Set RTC prescaler: set RTC period to 1 sec */
  RTC_SetPrescaler( 3199 ); /* RTC period = RTCCLK/RTC_PR = (32 KHz)/(31999+1) */
 
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  RTC_SetCounter(10);
  
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
}

void rtc_awaken_Init( void )
{
  /* Enable BKP clocks */
  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_BKP, ENABLE );

  /* Allow access to BKP Domain */
  PWR_BackupAccessCmd( ENABLE );

  /* NVIC configuration */
  NVIC_RTC_Configuration();

  RTC_Configuration();

  RCC_ClearResetFlags();
}

void RTCAlarm_IRQHandler( void )
{
  __awaken_();//唤醒时钟设置

  /* Clear the RTC Alarm Second interrupt */
  RTC_ClearITPendingBit( RTC_IT_ALR );
  /* Clear the  EXTI line 17 pending bit */
  EXTI_ClearITPendingBit( EXTI_Line17 );

}
