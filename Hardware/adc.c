#include "adc.h"
#include "stdio.h"

/* Private define ------------------------------------------------------------*/
#define CurrentVDD  3300
#define K           4432
#define TEMP1       30
#define TEMP1_VAL   787214

__IO uint16_t ADCConvertedValue[ADC_ConBufferLen][2];

void ADC_DMA_Init(void)
{
  DMAC_Channel_InitTypeDef DMAC_Channel_InitStruct;
  ADC_InitTypeDef ADC_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
  
  /* Enable DMAC1, ADC, GPIOA clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMAC1Bridge, ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_BMX1 |RCC_APB1Periph_DMAC1 |RCC_APB1Periph_ADC |RCC_APB1Periph_GPIOA, ENABLE);

  /* Configure PA3 (ADC Channel3) as analog mode */
  GPIO_Init(GPIOA, GPIO_Pin_2, GPIO_MODE_ANA);

  /* DMAC channel0 configuration ---------------------------------------------*/
  DMAC_DeInit(DMAC1);
  DMAC_Channel_InitStruct.DMAC_SourceBaseAddr = (uint32_t)&ADC->DR;
  DMAC_Channel_InitStruct.DMAC_DestinationBaseAddr = (uint32_t)ADCConvertedValue;
  DMAC_Channel_InitStruct.DMAC_Interrupt = DMAC_Interrupt_Disable;
  DMAC_Channel_InitStruct.DMAC_SourceTransferWidth = DMAC_SourceTransferWidth_16b;
  DMAC_Channel_InitStruct.DMAC_DestinationTransferWidth = DMAC_DestinationTransferWidth_16b;
  DMAC_Channel_InitStruct.DMAC_SourceAddrInc = DMAC_SourceAddrInc_NoChange;   //源地址不变
  DMAC_Channel_InitStruct.DMAC_DestinationAddrInc = DMAC_DestinationAddrInc_Increment;  //目标地址增加
  DMAC_Channel_InitStruct.DMAC_SourceTransactionLength = DMAC_SourceTransactionLength_1;
  DMAC_Channel_InitStruct.DMAC_DestinationTransactionLength = DMAC_DestinationTransactionLength_1;    /* Invalid for memory */
  DMAC_Channel_InitStruct.DMAC_TransferTypeAndFlowControl = DMAC_TransferTypeAndFlowControl_PeripheralToMemory_DMAC;
  DMAC_Channel_InitStruct.DMAC_SourceMasterInterface = DMAC_SourceMasterInterface_APB;
  DMAC_Channel_InitStruct.DMAC_DestinationMasterInterface = DMAC_DestinationMasterInterface_AHB;
  DMAC_Channel_InitStruct.DMAC_BlockTransferSize = ADC_ConBufferLen*2;  //传输长度    <=511
  DMAC_Channel_InitStruct.DMAC_SourceHandshakingInterfaceSelect = DMAC_SourceHandshakingInterfaceSelect_Hardware;
  DMAC_Channel_InitStruct.DMAC_DestinationHandshakingInterfaceSelect = DMAC_DestinationHandshakingInterfaceSelect_Hardware;
  DMAC_Channel_InitStruct.DMAC_SourceHandshakingInterfacePolarity = DMAC_SourceHandshakingInterfacePolarity_High;
  DMAC_Channel_InitStruct.DMAC_DestinationHandshakingInterfacePolarity = DMAC_DestinationHandshakingInterfacePolarity_High;
  DMAC_Channel_InitStruct.DMAC_AutomaticSourceReload = DMAC_AutomaticSourceReload_Disable;
  DMAC_Channel_InitStruct.DMAC_AutomaticDestinationReload = DMAC_AutomaticDestinationReload_Enable;  //自动重装载
  DMAC_Channel_InitStruct.DMAC_FlowControlMode = DMAC_FlowControlMode_0;
  DMAC_Channel_InitStruct.DMAC_FIFOMode = DMAC_FIFOMode_0;
  DMAC_Channel_InitStruct.DMAC_ChannelPriority = 0;
  DMAC_Channel_InitStruct.DMAC_ProtectionControl = 0x1;
  DMAC_Channel_InitStruct.DMAC_SourceHardwareHandshakingInterfaceAssign = DMAC_HardwareHandshakingInterface_ADC_Regular;
  DMAC_Channel_InitStruct.DMAC_DestinationHardwareHandshakingInterfaceAssign = 0;
  DMAC_Channel_InitStruct.DMAC_MaximumAMBABurstLength = 0;
  DMAC_Channel_Init(DMAC1, DMAC_Channel_0, &DMAC_Channel_InitStruct);
  
//  DMAC_ITConfig(DMAC1, DMAC_Channel_0, DMAC_IT_BLOCK, ENABLE);
 // DMAC_ITConfig(DMAC1, DMAC_Channel_0, DMAC_IT_DSTTRAN, ENABLE);
//  DMAC_ITConfig(DMAC1, DMAC_Channel_0, DMAC_IT_ERR, ENABLE);
  
  NVIC_InitStructure.NVIC_IRQChannel = DMAC1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  DMAC_Cmd(DMAC1, ENABLE);
  DMAC_ChannelCmd(DMAC1, DMAC_Channel_0, ENABLE);


  /* ADC configuration -------------------------------------------------------*/
  PWR_UnlockANA();
  ANCTL_SARADCCmd(ENABLE);
  ANCTL->BGCR2 |= 0x02;   //芯片内部温度
  PWR_LockANA();

  ADC_InitStructure.ADC_ScanConvMode = ENABLE;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfChannel = 2;
  ADC_Init(&ADC_InitStructure);

  /* ADC regular channel3 configuration */
  ADC_RegularChannelConfig(ADC_Channel_2, 1, ADC_SampleTime_7Cycles5);
  ADC_RegularChannelConfig(ADC_Channel_16, 2, ADC_SampleTime_239Cycles5);

  /* Enable ADC DMA */
  ADC_DMACmd(ENABLE);

  /* Enable ADC external trigger conversion */
  ADC_ExternalTrigConvCmd(ENABLE);

  /* Enable ADC */
  ADC_Cmd(ENABLE);

  /* Start ADC calibration */
  ADC_StartCalibration();
  /* Check the end of ADC calibration */
  while(ADC_GetCalibrationStatus());

  /* Enable ADC reset calibration register */
  ADC_ResetCalibration();
  /* Check the end of ADC reset calibration register */
  while(ADC_GetResetCalibrationStatus());

  /* Start ADC Software Conversion */
  ADC_SoftwareStartConvCmd(ENABLE);
}

/**
 * @brief  Get chip temperature .
 * @param  Value: Channel 16 ADC acquisition value .
 * @param  CurrentVDD: Chip supply voltage.
 * @return Centigrade value 
 */
float GetChipTemperature(uint32_t Value, uint32_t VDD)
{
  return (float)((TEMP1_VAL - ((float)Value * VDD * 1000 / 4096)) / K + TEMP1);
}

float GET_ADC_AverageValue(void)
{
  uint32_t ADC_SumValue=0,meter=0;
  
  for(meter=0;meter<ADC_ConBufferLen;meter++)
  {
    ADC_SumValue += ADC_GetADValue(ADCConvertedValue[meter][0]);
  }
  
  return (ADC_SumValue/ADC_ConBufferLen);   
}

float GET_ADC_TemperatureAvgVal(void)
{
  uint32_t ADC_SumValue=0,meter=0;
  
  for(meter=0;meter<ADC_ConBufferLen;meter++)
  {
    ADC_SumValue += GetChipTemperature(ADCConvertedValue[meter][1],CurrentVDD);
  }
  
  return (ADC_SumValue/ADC_ConBufferLen); 
}

void DMAC1_IRQHandler(void)
{
//  if(DMAC_GetITStatus(DMAC1, DMAC_Channel_0, DMAC_IT_BLOCK) != RESET)
//  {
//    DMAC_ClearITPendingBit(DMAC2, DMAC_Channel_0, DMAC_IT_BLOCK);
////    printf("DMA block transfer complete.\r\n");
//  }

  if(DMAC_GetITStatus(DMAC1, DMAC_Channel_0, DMAC_IT_DSTTRAN) != RESET)
  {
    DMAC_ClearITPendingBit(DMAC2, DMAC_Channel_0, DMAC_IT_TFR);
//    printf("DMA transfer complete.\r\n");
  }

//  if(DMAC_GetITStatus(DMAC1, DMAC_Channel_0, DMAC_IT_ERR) != RESET)
//  {
//    DMAC_ClearITPendingBit(DMAC2, DMAC_Channel_0, DMAC_IT_ERR);
////    printf("DMA transfer error!!!\r\n");
//  }
}
