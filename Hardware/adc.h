#ifndef __ADC_H
#define __ADC_H
#include "wb32f10x_it.h"

#define  ADC_ConBufferLen  (511>>1)

extern __IO uint16_t ADCConvertedValue[ADC_ConBufferLen][2];

void ADC_DMA_Init(void);
float GET_ADC_AverageValue(void);
float GET_ADC_TemperatureAvgVal(void);

#endif

