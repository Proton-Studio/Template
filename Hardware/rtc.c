#include "rtc.h"
#include "stdio.h"
#include "led.h"
#include "adc.h"
#include "gpio_led.h"
#include "systick.h"
RTC_StructDef RtcStruct = {0};
_calendar_obj calendar; /* Clock Struct */

#define DISLSI       /* ON/OFF LSI Clock          ON: ENLSI         OFF: DISLSI */
#define DISRTCALARM
/**
  * @brief  Configures the nested vectored interrupt controller.
  * @param  None
  * @retval None
  */
void NVIC_RTC_Configuration( void )
{

  NVIC_InitTypeDef NVIC_InitStructure;

#ifdef ENRTCALARM

  EXTI_InitTypeDef EXTI_InitStructure;

#endif

  /* Enable the RTC Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init( &NVIC_InitStructure );

#ifdef ENRTCALARM

  /* Configure EXTI line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init( &EXTI_InitStructure );

  /* Enable the RTC Alarm Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = RTCAlarm_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init( &NVIC_InitStructure );

#endif
}

void RTC_Configuration( void )
{

  /* Reset Backup Domain */
  BKP_DeInit();

#ifndef ENLSI

  /* Enable LSE */
  BKP_LSEConfig( BKP_LSE_ON );
  /* Wait till LSE is ready */
  while( BKP_GetLSEReadyFlagStatus() == RESET )
  {
    ;
  }
  /* Select LSE as RTC Clock Source */
  BKP_RTCCLKConfig( BKP_RTCCLKSource_LSE );

#else

  /*Unlock ANA*/
  PWR_UnlockANA();
  /* Enable LSI */
  ANCTL_LSICmd( ENABLE );
  /* Lock ANA */
  PWR_LockANA();
  while( ( ANCTL->LSISR & ANCTL_LSISR_LSIRDY ) == 0 )
  {
    ;
  }
  /* Select LSIas RTC Clock Source */
  BKP_RTCCLKConfig( BKP_RTCCLKSource_LSI );

#endif

  /* Enable RTC Clock */
  BKP_RTCCLKCmd( ENABLE );

  /* Wait for RTC registers synchronization */
  RTC_WaitForSynchro();

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  /* Enable the RTC Second */
  RTC_ITConfig( RTC_IT_SEC, ENABLE );

#ifdef ENRTCALARM

  RTC_ITConfig( RTC_IT_ALR, ENABLE );

#endif

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

#ifndef ENLSI

  /* Set RTC prescaler: set RTC period to 1 sec */
  RTC_SetPrescaler( 32767 ); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */

#else

  /* Set RTC prescaler: set RTC period to 1 sec */
  RTC_SetPrescaler( 31999 ); /* RTC period = RTCCLK/RTC_PR = (32 KHz)/(31999+1) */

#endif

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  /* Sets the RTC counter */
  RTC_Set( 2015, 1, 14, 17, 42, 55 ); //Set time

#ifdef ENRTCALARM

  RTC_WaitForLastTask();
  RTC_SetAlarm( RTC_GetCounter() + 5 );

#endif

}

void Rtc_Init( void )
{
  /* Enable BKP clocks */
  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_BKP, ENABLE );

  /* Allow access to BKP Domain */
  PWR_BackupAccessCmd( ENABLE );

  /* NVIC configuration */
  NVIC_RTC_Configuration();

  if( BKP_ReadBackupRegister( BKP_DR1 ) != 0x55AA )
  {
    /* Backup data register value is not correct or not yet programmed (when
       the first time the program is executed) */

    printf( "RTC not yet configured...\r\n" );

    /* RTC Configuration */
    RTC_Configuration();

    printf( "RTC configured...\r\n" );

    BKP_WriteBackupRegister( BKP_DR1, 0x55AA );
  }
  else
  {
    /* Check if the Power On Reset flag is set */
    if( RCC_GetResetFlagStatus( RCC_RSTFLAG_PORRST ) != RESET )
    {
      printf( "Power On Reset occurred...\r\n" );
    }
    /* Check if the Pin Reset flag is set */
    else if( RCC_GetResetFlagStatus( RCC_RSTFLAG_PINRST ) != RESET )
    {
      printf( "External Reset occurred...\r\n" );
    }

    printf( "No need to configure RTC...\r\n" );
    /* Wait for RTC registers synchronization */
    RTC_WaitForSynchro();

    /* Enable the RTC Second */
    RTC_ITConfig( RTC_IT_SEC, ENABLE );

#ifdef   ENRTCALARM

    RTC_ITConfig( RTC_IT_ALR, ENABLE );

#endif

    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();

#ifndef ENLSI

    /* Set RTC prescaler: set RTC period to 1 sec */
    RTC_SetPrescaler( 32767 ); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */

#else

    /* Set RTC prescaler: set RTC period to 1 sec */
    RTC_SetPrescaler( 31999 ); /* RTC period = RTCCLK/RTC_PR = (32 KHz)/(31999+1) */

#endif

    RTC_WaitForLastTask();

#ifdef   ENRTCALARM

    RTC_SetAlarm( RTC_GetCounter() + 5 );

#endif

  }

  /* Clears the RCC reset flags. */
  RCC_ClearResetFlags();
}

void RTC_IRQHandler( void )
{
  float temperature;
  if( RTC_GetITStatus( RTC_IT_SEC ) != RESET )
  {
    /* Clear the RTC Second interrupt */
    RTC_ClearITPendingBit( RTC_IT_SEC );

    RTC_Get();

    temperature = ( float )GET_ADC_TemperatureAvgVal();

    LED_SetSegmentCode( 0, table[( ( int )temperature ) / 10 % 10] );
    LED_SetSegmentCode( 1, table[( ( int )temperature ) % 10] );
    LED_SetSegmentCode( 2, 0x63 );  //℃
    LED_SetSegmentCode( 3, 0x39 );

//    LED_SetSegmentCode( 0, table[calendar.hour / 10 % 10] );
//    LED_SetSegmentCode( 1, table[calendar.hour % 10] );
//    LED_SetSegmentCode( 2, table[calendar.min / 10 % 10] );
//    LED_SetSegmentCode( 3, table[calendar.min % 10] );

    /* Enable time update */
    RtcStruct.TimeDisplay = 1;
  }
}

#ifdef ENRTCALARM

void RTCAlarm_IRQHandler( void )
{
  if( RTC_GetFlagStatus( RTC_IT_ALR ) != RESET )
  {
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();
    RTC_SetAlarm( RTC_GetCounter() + 5 );

    /* Clear the RTC Alarm Second interrupt */
    RTC_ClearITPendingBit( RTC_IT_ALR );
    /* Clear the  EXTI line 17 pending bit */
    EXTI_ClearITPendingBit( EXTI_Line17 );
  }
}

#endif

//判断是否是闰年函数
//月份   1  2  3  4  5  6  7  8  9  10 11 12
//闰年   31 29 31 30 31 30 31 31 30 31 30 31
//非闰年 31 28 31 30 31 30 31 31 30 31 30 31
//输入:年份
//输出:该年份是不是闰年.1,是.0,不是
uint8_t Is_Leap_Year( uint16_t year )
{
  if( year % 4 == 0 ) //必须能被4整除
  {
    if( year % 100 == 0 )
    {
      if( year % 400 == 0 )
        return 1; //如果以00结尾,还要能被400整除
      else
        return 0;
    }
    else
      return 1;
  }
  else
    return 0;
}
//设置时钟
//把输入的时钟转换为秒钟
//以1970年1月1日为基准
//1970~2099年为合法年份
//返回值:0,成功;其他:错误代码.
//月份数据表
uint8_t const table_week[12] = {0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5}; //月修正数据表
//平年的月份日期表
const uint8_t mon_table[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
uint8_t RTC_Set( uint16_t syear, uint8_t smon, uint8_t sday, uint8_t hour, uint8_t min, uint8_t sec )
{
  uint16_t t;
  uint32_t seccount = 0;
  if( syear < 1970 || syear > 2099 || smon == 0 || sday == 0 )
    return 1;
  if( smon > 12 || sday > 31 || hour >= 24 || min >= 60 || sec >= 60 )
    return 2;
  for( t = 1970; t < syear; t++ ) //把所有年份的秒钟相加
  {
    if( Is_Leap_Year( t ) )
      seccount += 31622400; //闰年的秒钟数
    else
      seccount += 31536000; //平年的秒钟数
  }
  smon -= 1;
  for( t = 0; t < smon; t++ ) //把前面月份的秒钟数相加
  {
    seccount += ( uint32_t )mon_table[t] * 86400; //月份秒钟数相加
    if( Is_Leap_Year( syear ) && t == 1 )
      seccount += 86400; //闰年2月份增加一天的秒钟数
  }
  seccount += ( uint32_t )( sday - 1 ) * 86400; //把前面日期的秒钟数相加
  seccount += ( uint32_t )hour * 3600;      //小时秒钟数
  seccount += ( uint32_t )min * 60;         //分钟秒钟数
  seccount += sec;                          //最后的秒钟加上去

  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_BKP, ENABLE ); //BKP外设时钟
  PWR_BackupAccessCmd( ENABLE );                    //使能RTC和后备寄存器访问
  RTC_SetCounter( seccount );                       //设置RTC计数器的值

  RTC_WaitForLastTask(); //等待最近一次对RTC寄存器的写操作完成
  return 0;
}

//初始化闹钟
//以1970年1月1日为基准
//1970~2099年为合法年份
//syear,smon,sday,hour,min,sec：闹钟的年月日时分秒
//返回值:0,成功;其他:错误代码.
uint8_t RTC_Alarm_Set( uint16_t syear, uint8_t smon, uint8_t sday, uint8_t hour, uint8_t min, uint8_t sec )
{
  uint16_t t;
  uint32_t seccount = 0;
  if( syear < 1970 || syear > 2099 )
    return 1;
  for( t = 1970; t < syear; t++ ) //把所有年份的秒钟相加
  {
    if( Is_Leap_Year( t ) )
      seccount += 31622400; //闰年的秒钟数
    else
      seccount += 31536000; //平年的秒钟数
  }
  smon -= 1;
  for( t = 0; t < smon; t++ ) //把前面月份的秒钟数相加
  {
    seccount += ( uint32_t )mon_table[t] * 86400; //月份秒钟数相加
    if( Is_Leap_Year( syear ) && t == 1 )
      seccount += 86400; //闰年2月份增加一天的秒钟数
  }
  seccount += ( uint32_t )( sday - 1 ) * 86400; //把前面日期的秒钟数相加
  seccount += ( uint32_t )hour * 3600;      //小时秒钟数
  seccount += ( uint32_t )min * 60;         //分钟秒钟数
  seccount += sec;                          //最后的秒钟加上去
  //设置时钟
  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_BKP, ENABLE ); //使能BKP外设时钟
  PWR_BackupAccessCmd( ENABLE );                    //使能后备寄存器访问
  //上面三步是必须的!

  RTC_SetAlarm( seccount );

  RTC_WaitForLastTask(); //等待最近一次对RTC寄存器的写操作完成

  return 0;
}

//得到当前的时间
//返回值:0,成功;其他:错误代码.
uint8_t RTC_Get( void )
{
  static uint16_t daycnt = 0;
  uint32_t timecount = 0;
  uint32_t temp = 0;
  uint16_t temp1 = 0;
  timecount = RTC_GetCounter();
  temp = timecount / 86400; //得到天数(秒钟数对应的)
  if( daycnt != temp )      //超过一天了
  {
    daycnt = temp;
    temp1 = 1970; //从1970年开始
    while( temp >= 365 )
    {
      if( Is_Leap_Year( temp1 ) ) //是闰年
      {
        if( temp >= 366 )
          temp -= 366; //闰年的秒钟数
        else
        {
          temp1++;
          break;
        }
      }
      else
        temp -= 365; //平年
      temp1++;
    }
    calendar.w_year = temp1; //得到年份
    temp1 = 0;
    while( temp >= 28 ) //超过了一个月
    {
      if( Is_Leap_Year( calendar.w_year ) && temp1 == 1 ) //当年是不是闰年/2月份
      {
        if( temp >= 29 )
          temp -= 29; //闰年的秒钟数
        else
          break;
      }
      else
      {
        if( temp >= mon_table[temp1] )
          temp -= mon_table[temp1]; //平年
        else
          break;
      }
      temp1++;
    }
    calendar.w_month = temp1 + 1; //得到月份
    calendar.w_date = temp + 1;   //得到日期
  }
  temp = timecount % 86400;                                                         //得到秒钟数
  calendar.hour = temp / 3600;                                                      //小时
  calendar.min = ( temp % 3600 ) / 60;                                              //分钟
  calendar.sec = ( temp % 3600 ) % 60;                                              //秒钟
  calendar.week = RTC_Get_Week( calendar.w_year, calendar.w_month, calendar.w_date ); //获取星期
  return 0;
}
//获得现在是星期几
//功能描述:输入公历日期得到星期(只允许1901-2099年)
//输入参数：公历年月日
//返回值：星期号
uint8_t RTC_Get_Week( uint16_t year, uint8_t month, uint8_t day )
{
  uint16_t temp2;
  uint8_t yearH, yearL;

  yearH = year / 100;
  yearL = year % 100;
  // 如果为21世纪,年份数加100
  if( yearH > 19 )
    yearL += 100;
  // 所过闰年数只算1900年之后的
  temp2 = yearL + yearL / 4;
  temp2 = temp2 % 7;
  temp2 = temp2 + day + table_week[month - 1];
  if( yearL % 4 == 0 && month < 3 )
    temp2--;
  return ( temp2 % 7 );
}
